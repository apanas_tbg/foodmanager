package foodmanager;

public class Employee {
    private int EmployeeID;  
    private String LastName;
    private String FirstName;
    private String Title;
    private String TitleOfCourtesy;
    private java.util.Date BirthDate;
    private java.util.Date HireDate;
    private String Address;
    private String City;
    private String Region;
    private String PostalCode;
    private String Country;
    private String HomePhone;
    private String Extension;
    private String Photo;
    private String Notes;
    private int ReportsTo;

    public int getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(int id) {
        this.EmployeeID = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String value) {
        this.LastName = value;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String value) {
        this.FirstName = value;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String value) {
        this.Title = value;
    }

    public String getTitleOfCourtesy() {
        return TitleOfCourtesy;
    }

    public void setTitleOfCourtesy(String value) {
        this.TitleOfCourtesy = value;
    }

    public java.util.Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(java.util.Date value) {
        this.BirthDate = value;
    }

    public java.util.Date getHireDate() {
        return HireDate;
    }

    public void setHireDate(java.util.Date value) {
        this.HireDate = value;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String value) {
        this.Address = value;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String value) {
        this.City = value;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String value) {
        this.Region = value;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String value) {
        this.PostalCode = value;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String value) {
        this.Country = value;
    }

    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String value) {
        this.HomePhone = value;
    }

    public String getExtension() {
        return Extension;
    }

    public void setExtension(String value) {
        this.Extension = value;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String value) {
        this.Photo = value;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String value) {
        this.Notes = value;
    }

    public int getReportsTo() {
        return ReportsTo;
    }

    public void setReportsTo(int value) {
        this.ReportsTo = value;
    }
}
