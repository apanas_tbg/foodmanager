package foodmanager;

import java.util.Calendar;

public class Order {
    private int OrderID;
    private String CustomerID; // \note in SQL file its type is 'varchar(5)'
    private int EmployeeID;
    private int ShipVia;
    private float Freight;
    private String ShipName;
    private String ShipAddress;
    private String ShipCity;
    private String ShipRegion;
    private String ShipPostalCode;
    private String ShipCountry;

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int id) {
        this.OrderID = id;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String id) {
        this.CustomerID = id;
    }

    public int getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(int id) {
        this.EmployeeID = id;
    }

    public int getShipVia() {
        return ShipVia;
    }

    public void setShipVia(int ShipVia) {
        this.ShipVia = ShipVia;
    }

    public float getFreight() {
        return Freight;
    }

    public void setFreight(float Freight) {
        this.Freight = Freight;
    }

    public String getShipName() {
        return ShipName;
    }

    public void setShipName(String ShipName) {
        this.ShipName = ShipName;
    }

    public String getShipAddress() {
        return ShipAddress;
    }

    public void setShipAddress(String ShipAddress) {
        this.ShipAddress = ShipAddress;
    }

    public String getShipCity() {
        return ShipCity;
    }

    public void setShipCity(String ShipCity) {
        this.ShipCity = ShipCity;
    }

    public String getShipRegion() {
        return ShipRegion;
    }

    public void setShipRegion(String ShipRegion) {
        this.ShipRegion = ShipRegion;
    }

    public String getShipPostalCode() {
        return ShipPostalCode;
    }

    public void setShipPostalCode(String ShipPostalCode) {
        this.ShipPostalCode = ShipPostalCode;
    }

    public String getShipCountry() {
        return ShipCountry;
    }

    public void setShipCountry(String ShipCountry) {
        this.ShipCountry = ShipCountry;
    }
}
