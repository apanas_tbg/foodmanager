package foodmanager;

public class Category {
    private int CategoryId;  
    private String CategoryName;
    private String Description;
    private String Picture;

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int id) {
        this.CategoryId = id;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String Picture) {
        this.Picture = Picture;
    }
}
