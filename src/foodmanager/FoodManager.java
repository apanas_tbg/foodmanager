package foodmanager;

import java.util.List;
import java.util.Vector;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;

public class FoodManager extends javafx.application.Application {
    public static void main(String[] args) {
        launch(args);
    }

    private static SessionFactory m_sessionFactory; 

    @Override
    public void start(Stage primaryStage) {
        TabPane tabPane = new TabPane();

        try {
            m_sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        }
        catch (Throwable ex) { 
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex); 
        }

        populateTabPane(
            tabPane,
            "Employees",
            new String[] {
                "First Name",
                "Last Name",
                "Title",
                "Home Phone",
            },
            Arrays.asList(
                new PropertyValueFactory<Employee, String>("FirstName"),
                new PropertyValueFactory<Employee, String>("LastName"),
                new PropertyValueFactory<Employee, String>("Title"),
                new PropertyValueFactory<Employee, String>("HomePhone")
            ),
            "foodmanager.Employee"
        );

        populateTabPane(
            tabPane,
            "Categories",
            new String[] {
                "CategoryName",
                "Description",
            },
            Arrays.asList(
                new PropertyValueFactory<Employee, String>("CategoryName"),
                new PropertyValueFactory<Employee, String>("Description")
            ),
            "foodmanager.Category"
        );

        populateTabPane(
            tabPane,
            "Customers",
            new String[] {
                "CustomerID",
                "CompanyName",
                "City",
                "Country",
                "Phone"
            },
            Arrays.asList(
                new PropertyValueFactory<Customer, String>("CustomerID"),
                new PropertyValueFactory<Customer, String>("CompanyName"),
                new PropertyValueFactory<Customer, String>("City"),
                new PropertyValueFactory<Customer, String>("Country"),
                new PropertyValueFactory<Customer, String>("Phone")
            ),
            "foodmanager.Customer"
        );

        populateTabPane(
                tabPane,
                "Orders",
                new String[] {
                    "CustomerID",
                    "ShipName",
                    "ShipAddress",
                    "ShipCity",
                    "ShipRegion",
                    "ShipPostalCode",
                    "ShipCountry"
                },
                Arrays.asList(
                    new PropertyValueFactory<Customer, String>("CustomerID"),
                    new PropertyValueFactory<Customer, String>("ShipName"),
                    new PropertyValueFactory<Customer, String>("ShipAddress"),
                    new PropertyValueFactory<Customer, String>("ShipCity"),
                    new PropertyValueFactory<Customer, String>("ShipRegion"),
                    new PropertyValueFactory<Customer, String>("RequiredDate"),
                    new PropertyValueFactory<Customer, String>("ShipPostalCode"),
                    new PropertyValueFactory<Customer, String>("ShipCountry")
                ),
                "foodmanager.Order"
            );

        primaryStage.setTitle("FoodManager");

        primaryStage.setScene(new Scene(tabPane, 920, 450));
        primaryStage.show();
	}

    private static <ObjectType> void populateTabPane(
        TabPane tabPane,
        String tabTitle,
        String[] columnsData,
        List<PropertyValueFactory<ObjectType, ?>> propertyFactories,
        String className)
    {
        TableView<ObjectType> table = new TableView<ObjectType>();
        table.setEditable(true);
        ObservableList columnsOfTable = table.getColumns();
        int index = 0;
        for (String currentColumnData : columnsData) {
            TableColumn currentColumn = new TableColumn(currentColumnData);
            currentColumn.setPrefWidth(150);
            currentColumn.setCellFactory(TextFieldTableCell.<ObjectType>forTableColumn());
            currentColumn.setCellValueFactory((PropertyValueFactory) propertyFactories.get(index));
            currentColumn.setOnEditCommit(
                new EventHandler<CellEditEvent<ObjectType, String>>() {
                    @Override
                    public void handle(CellEditEvent<ObjectType, String> t) {
                        System.out.println("Clicked");
                        ((Employee) t
                            .getTableView()
                            .getItems()
                            .get(
                                t
                                    .getTablePosition()
                                    .getRow()
                                )
                        )
                        .setFirstName(t.getNewValue());
                    }
                }
            );

            columnsOfTable.add(currentColumn);
            ++index;
        }

        Session session = m_sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            ObservableList<ObjectType> cellData =
                FXCollections.observableArrayList(session.createCriteria(className).list());
            table.setItems(cellData);
        }
        catch (Exception e) {
           if (tx!=null) tx.rollback();
           e.printStackTrace();
           session.close();
        }

        VBox verticalBox = new VBox();
        Button addButton = new Button("+");
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                ObservableList<ObjectType> cellData =
                    FXCollections.observableArrayList(session.createCriteria(className).list());

                try {
                    ObjectType newElement = (ObjectType) Class
                        .forName(className)
                        .getConstructor()
                        .newInstance(/* eventually parameters - here */);
                    cellData.add(newElement);
                }
                catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        verticalBox.getChildren().add(addButton);
        // Don't want to close session to be able to add new fields
        //session.close();

        BorderPane border = new BorderPane();
        border.setRight(verticalBox);
        border.setCenter(table);

        Tab tab = new Tab(tabTitle);
        tab.setContent(border);
        tab.setClosable(false);
        tabPane.getTabs().add(tab);
    }
}
